# FOSS VR Testing Requests

Due to the expense of VR hardware, it can be difficult for a hobbyist developer
to get their code tested on a variety of VR headsets, OpenXR runtimes, and
graphics cards.

This page lists specific testing requests from FOSS VR developers which they
may be unable to do themselves due to lack of necessary hardware or software
setup. If you have a VR headset and want to help improve VR support in FOSS
projects, please dive in and test and report back to the developers.

The Matrix room
[#foss-vr-testing:hoganfam.uk](https://matrix.to/#/#foss-vr-testing:hoganfam.uk)
is dedicated to discussion of VR testing for FOSS projects.

Information that is helpful to provide to the developers when testing:
* VR Headset type
* OpenXR runtime and version
* Graphics card and driver version
* OS / Linux distribution
* What worked and what didn't work

Information that is helpful to provide to testers when requesting testing:
* Contact details
* Desired hardware / software combinations for testing
* Instructions for fetching and building the code
* Specific guidance on what should be tested

## Contents
[TOC]

## [osgXR](projects.md#osgxr), [FlightGear](projects.md#flightgear)
* Contact: [James](https://matrix.to/#/@james:hoganfam.uk) in
  [#foss-vr-testing:hoganfam.uk](https://matrix.to/#/#foss-vr-testing:hoganfam.uk),
  the [Official FlightGear Matrix Room](https://wiki.flightgear.org/Matrix#Official_FlightGear_Matrix_Room),
  or [#osgxr:hoganfam.uk](https://matrix.to/#/#osgxr:hoganfam.uk)

### Windows support
* Requirements: Windows, Headset not requiring WMR
* Difficulty: Easy
* Reason: [FlightGear](projects.md#flightgear) supports Windows, developer was
  able to port [osgXR](projects.md#osgxr) to Windows but unable to test with a
  headset
* Importance: Unknown, nobody has requested it yet

The following needs doing
* [ ] On Windows, build OpenXR-SDK, and then next branch of
  [FlightGear](projects.md#flightgear), making sure VR support gets enabled
* [ ] Test with real headset (rather than SteamVR null driver)
