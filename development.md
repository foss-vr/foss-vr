# FOSS VR Development Requests

Due to the expense of VR hardware, it can be difficult for a hobbyist developer
to implement certain for a range of VR headsets and controllers.

This page lists specific development requests from FOSS VR developers which
they may be unable to do themselves for whatever reason (lack of necessary
hardware, software setup or motivation).

If you have a VR headset and want to get involved in some light VR development,
do have a look and contact the developer if you think you can help.

The Matrix room
[#foss-vr-dev:hoganfam.uk](https://matrix.to/#/#foss-vr-dev:hoganfam.uk)
is dedicated to discussion of VR development for FOSS projects.

Developers, please include the following information in your requests:
* Contact details
* Required hardware & software
* Difficulty level
* Importance (have users been requesting it?)
* What's involved

## Contents
[TOC]

## [osgXR](projects.md#osgxr), [FlightGear](projects.md#flightgear)
* Contact: [James](https://matrix.to/#/@james:hoganfam.uk) in
  [#foss-vr-dev:hoganfam.uk](https://matrix.to/#/#foss-vr-dev:hoganfam.uk),
  the [Official FlightGear Matrix Room](https://wiki.flightgear.org/Matrix#Official_FlightGear_Matrix_Room),
  or [#osgxr:hoganfam.uk](https://matrix.to/#/#osgxr:hoganfam.uk)

### WMR support
* Requirements: Windows, WMR headset
* Difficulty: Medium
* Reason: [FlightGear](projects.md#flightgear) supports Windows, developer
  doesn't use Windows
* Importance: Unknown, nobody has requested WMR support yet

The following needs doing
* [ ] Add DirectX graphics binding support to osgXR, using
  WGL\_NV\_DX\_interop/WGL\_NV\_DX\_interop2 extension to do the resource sharing
* [ ] Get it working with [FlightGear](projects.md#flightgear)

References
* [Blender](projects.md#blender) can do it, see
  [Core Support of Virtual Reality Headsets through OpenXR - GSoC Final Report](https://wiki.blender.org/wiki/User:Severin/GSoC-2019/Final_Report)
  and linked patch.
* [OpenMW](projects.md#openmw) can do it, see
  [OpenMW-VR Project](https://gitlab.com/madsbuvi/openmw)
