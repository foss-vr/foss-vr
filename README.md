# FOSS VR

This project aims to help support development efforts to add and improve VR
support in free & open source software (FOSS) projects.

It aims to provide:
* A place to discuss & document existing FOSS projects and games which could be
  improved with VR support, and new VR FOSS project/game ideas.
* A place for users with VR hardware to be able to directly help FOSS VR development
  efforts with testing, and for FOSS VR developers to find willing testers with
  a variety of hardware and software setups.
* A place for developers to discuss & document technical aspects of FOSS VR
  development, and particularly to support those wanting to add VR support to
  existing FOSS projects.

So if you care about improving VR support in FOSS projects, please join our
Matrix space at
[#foss-vr:hoganfam.uk](https://matrix.to/#/#foss-vr:hoganfam.uk).

## Topics

* [FOSS Projects](projects.md)
* [FOSS VR Testing Requests](testing.md)
* [FOSS VR Development Requests](development.md)
* Developer Documentation
  * [Adding VR Support to an Existing FOSS Project](dev/adding-vr-to-existing-project.md)
  * [Nvidia Proprietary Driver Notes](dev/nvidia.md)
  * [OpenXR Resources](dev/openxr/resources.md)
  * [OpenXR Hardware & Software Configuration Information](dev/openxr/xrinfo.md)
  * [Monado OpenXR Workarounds](dev/openxr/monado.md)
  * [SteamVR OpenXR Workarounds](dev/openxr/steamvr.md)
