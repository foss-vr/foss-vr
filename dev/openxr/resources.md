# OpenXR Resources

* [OpenXR Website](https://www.khronos.org/OpenXR/)
* [API Specifications](https://www.khronos.org/registry/OpenXR/#apispecs)
* [Monado OpenXR Runtime](https://monado.freedesktop.org/)
* [Monado OpenXR Resources](https://monado.freedesktop.org/openxr-resources.html)
