# SteamVR OpenXR Workarounds

SteamVR's proprietary OpenXR runtime has some annoying issues on Linux, some of
which may require workarounds in the application.

[TOC]

## <s>[Issue 523](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/523)</s> (until 1.26.2): `xrGetInstanceProperties` reports `runtimeVersion` of 0.1.0

All versions of SteamVR supporting OpenXR prior to SteamVR 1.26.2 report an
OpenXR `runtimeVersion` from `xrGetInstanceProperties` of 0.1.0. Since SteamVR
1.26.2 they report the SteamVR version number.

Bear this in mind when implementing version checks for workarounds.

## <s>[Issue 421](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/421)</s> (until 1.25): Certain OpenXR functions change current OpenGL context

The implementation of certain OpenXR functions in versions of SteamVR since
around 1.16.2 and until around 1.25.1 on Linux will switch and then clear the
current OpenGL context. This can result in unusual results from OpenGL
functions, such as the generation of objects such as textures failing.

This can be worked around by changing the context back in the application with
`glXMakeCurrent()` after these calls. Care must be taken if these functions are
called from different threads to ensure that the context is not active on
multiple threads concurrently, so it may be preferable to check the OpenGL
context prior to the call using `glXGetCurrentContext()` and restore it
afterwards if previously set.

The workaround is required for the following functions, regardless of
success/failure of the call:

* `xrEndFrame`
* `xrAcquireSwapchainImage`
* `xrWaitSwapchainImage`
* `xrReleaseSwapchainImage`
* `xrCreateSwapchain`

Note: SteamVR `linux_v1.14` and presumably other versions prior to around
1.16.2 don't appear to clear the context, but do still switch context, so if
you call these functions from a non-render thread you may have to release the
OpenGL context if not previously set. This is more likely to affect
`xrCreateSwapchain` than the others.

Note: Related workarounds are required for
[Monado Issue 145](monado.md#issue-145-xrcreatesession-assumes-opengl-context-is-current).

The best way to debug SteamVR's GL context changes is using ltrace, i.e.:
```
ltrace -f -e 'xr*' -e glXMakeCurrent $COMMAND
```

That way you can see what library calls SteamVR is making during which OpenXR
functions. The `-f` makes it follow forks to child threads which is useful for
multithreaded applications.  In the example below you can see SteamVR
(`vrclient.so`) switching and clearing the OpenGL context, then the workaround
(via libosgViewer) setting it back:
```
[pid 258359] libosgXR.so.4->xrAcquireSwapchainImage(0x2e8698f7bf0, 0, 0x6d07fc939804, 1 <unfinished ...>
[pid 258359] vrclient.so->glXMakeCurrent(0x2e83dad3830, 0x5000003, 0x2e83de9d1d8, 0)            = 1
[pid 258359] vrclient.so->glXMakeCurrent(0x2e83dad3830, 0, 0, 0x2e862d4f040)                    = 1
[pid 258359] <... xrAcquireSwapchainImage resumed> )                                            = 0
[pid 258359] libosgViewerrd.so.161->glXMakeCurrent(0x2e83dad3830, 0x5000003, 0x2e83de9d1d8, 0)  = 1
```

References:
* [gl_context_fix_layer](https://github.com/ChristophHaag/gl_context_fix_layer):
  An OpenXR layer specifically to attempt to work around this issue for
  applications that are missing the workaround (Note that this may not be what
  the application actually expects).

## [Issue 422](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/422): OpenXR applications hang instead of quit

The use of `xrDestroyInstance`, or an attempt to exit an application which has
already started using OpenXR, will just hang it. The `linux_v1.14` version
works but not with NVidia GPUs.

There doesn't appear to be a convenient workaround for cleanly quitting the
application other than killing it forcefully. If your application needs to be
able to turn VR off again it can still end the OpenXR session but keep the
instance alive, to delay the hang until application exit.

## [Issue 461](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/461): OpenXR app killed on user exit

If you need to be able to deactivate VR while continuing the application in VR,
you may hit problems with SteamVR wanting to kill the application when it is
closed or if the user switches to another VR application, even if the OpenXR
session has been destroyed.

Its possible that destroying the instance would help, but due to [issue
422](#issue-422-openxr-applications-hang-instead-of-quit) I can't tell. SteamVR
should really report instance lost to the application, to allow it to quit
cleanly itself or continue running without VR.

## [Issue 479](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/479): Unclean shutdown of OpenXR app breaks all OpenXR apps until SteamVR restarted

This seems to be triggered by an unclean shutdown of an OpenXR app, i.e. a
SIGKILL or SIGINT. It results in all future OpenXR applications failing with a
segmentation fault in `LfMutexUnlockRobust`, until SteamVR is restarted.

Avoid unclean shutdowns, even if it means hitting [issue
422](#issue-422-openxr-applications-hang-instead-of-quit) and then killing the
application.
