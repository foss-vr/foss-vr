# Monado OpenXR Workarounds

Some older versions of the open source Monado OpenXR runtime have issues that
require application workarounds.

[TOC]

## <s>[Issue 145](https://gitlab.freedesktop.org/monado/monado/-/issues/145)</s>: xrCreateSession assumes OpenGL context is current

The implementation of certain OpenXR functions on X11 in Monado versions up to
and including 21.0.0 will assume that the OpenGL context is current.

This can be worked around by changing the context in the application with
`glXMakeCurrent()` before these calls, and reverting the context again after
the call. Care must be taken if these functions are called from different
threads to ensure that the context is not active on multiple threads
concurrently, so it may be preferable to check the OpenGL context prior to the
call using `glXGetCurrentContext()` and restore or clear it afterwards if
necessary.

The workaround is most likely to be required for the following functions:

* `xrCreateSession`
* `xrCreateSwapchain`

Note: Related workarounds are required for older versions of SteamVR due to
[SteamVR Issue 421](steamvr.md#issue-421-until-125-certain-openxr-functions-change-current-opengl-context).
