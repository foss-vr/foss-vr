# Nvidia Proprietary Driver Notes

This page documents issues worth noting about Nvidia's proprietary graphics
drivers when used for VR on Linux.

[TOC]

## At least driver version 470 is required for asynchronous reprojection to work on SteamVR

Asynchronous reprojection allows an old frame to be reprojected if the VR
application is struggling to keep up with the frame rate of the HMD. This does
a lot to alleviate nausea when looking around with a laggy application.

However it requires high priority compute queue support in the driver, which
Nvidia finally supports since driver version 470. Be sure to use at least that
version to get asynchronous reprojection.

## [Monado Issue 122](https://gitlab.freedesktop.org/monado/monado/-/issues/122): Seg fault in nvidia 460 to 510 (fixed in 515)

Monado unfortunately triggers [a
crash](https://forums.developer.nvidia.com/t/linux-vkqueuepresentkhr-segmentation-fault-on-vk-ext-acquire-xlib-display-swapchain/191161)
in nvidia driver versions 460..510. It was eventually fixed in 515.

Prior to version 515 the only workaround was to use an older driver (may not be
trivial depending on your Linux distribution, and may lose you asynchronous
reprojection support), or to use extended mode on X11 with
`XRT_COMPOSITOR_FORCE_XCB=1 XRT_COMPOSITOR_XCB_FULLSCREEN=1 monado-service`, as
described in the Monado bug report, which will provide a somewhat degraded
experience.
