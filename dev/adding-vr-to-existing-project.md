# Adding VR Support to an Existing FOSS Project

Here are some suggestions on the best approach for adding VR support to an
existing FOSS project (assuming you've already decided it would be helpful).
* Check whether anybody has already attempted to add VR support before, and
  whether any of their code is usable.
* Use the OpenXR API, as it will provide the best portability.
* If the project uses an underlying graphics or game engine, consider adding VR
  support to it first (or as a library which interacts with it) so that other
  projects using the same engine can take advantage of it too.
* Communicate with the developers of the FOSS project and familiarise yourself
  with their development practices.
* Aim to release your changes early and often, to get early feedback.
* Use the same open source license as the upstream project, and aim to get your
  changes merged into the upstream project. This will allow users to more
  easily use your changes, and will help prevent your changes bitrotting.
