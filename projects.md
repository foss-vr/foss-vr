# FOSS Projects

## Contents
[TOC]

## Applications

### Blender

* [VR Scene Inspection Documentation](https://docs.blender.org/manual/en/latest/addons/3d_view/vr_scene_inspection.html)
* [Core Support of Virtual Reality Headsets through OpenXR - GSoC Final Report](https://wiki.blender.org/wiki/User:Severin/GSoC-2019/Final_Report)

## Graphics & Game Engines

### Irrlicht
Irrlicht is used by the following projects:
* [Minetest](#minetest) ([forked](https://github.com/minetest/irrlicht))
* [SuperTuxKart](#supertuxkart)

Links:
* [Irrlicht Website](https://irrlicht.sourceforge.io/)
* [HaeckseAlexandra's OpenXR fork](https://codeberg.org/LibreVR/irrlichtvr)

### OpenSceneGraph
OpenSceneGraph is used by the following projects:
* [FlightGear](#flightgear)
* [OpenMW](#openmw)

It is now in maintenance mode with VulkanSceneGraph being developed as its
natural successor. As such the addition of VR support directly into
OpenSceneGraph was not welcomed by its developers.

Instead the [osgXR](#osgXR) library has been written to allow OpenSceneGraph
applications to more easily implement VR support.

Links:
* [OpenSceneGraph Website](http://www.openscenegraph.org/)

#### osgXR
The osgXR library aims to ease the integration of OpenXR with
[OpenSceneGraph](#openscenegraph) applications.

It supports Linux and Windows, but not WMR (which only supports Direct3D
graphics bindings).
Other developers are [invited](development.md#osgxr-flightgear) to add WMR
support.

It is used by [FlightGear](#flightgear).

Links:
* [osgXR Project](https://github.com/amalon/osgXR)

## Games

### FlightGear
FlightGear uses [OpenSceneGraph](#openscenegraph).

VR support is in development via the [osgXR](#osgxr) library.

Like osgXR it supports VR on Linux and Windows, but not WMR (which only
supports Direct3D graphics bindings).
Other developers are [invited](development.md#osgxr-flightgear) to add WMR
support.

Links:
* [FlightGear Website](https://www.flightgear.org/)
* [FlightGear VR Project](https://wiki.flightgear.org/Virtual_Reality)

### Minetest
Minetest uses [a fork](https://github.com/minetest/irrlicht) of the [Irrlicht
engine](#irrlicht).

Links:
* [Minetest Website](https://www.minetest.net/)
* [VR support issue](https://github.com/minetest/minetest/issues/8586)
* [DonFlymoor's OpenXR fork](https://github.com/DonFlymoor/minetest-openXR)
* [HaeckseAlexandra's OpenXR fork](https://codeberg.org/LibreVR/minetestvr)

### OpenMW
OpenMW uses [OpenSceneGraph](#openscenegraph).

OpenMW has a fork to add VR support. It was started before the development of
osgXR and uses OpenXR directly.

Links:
* [OpenMW Website](https://openmw.org)
* [OpenMW-VR Project](https://gitlab.com/madsbuvi/openmw)

### SuperTuxKart
SuperTuxKart uses [Irrlicht](#irrlicht).

I'm not aware of any attempts to add VR support, but the development of VR
support for [Irrlicht](#irrlicht) / [Minetest](#Minetest) may bring it a step
closer.

Links:
* [SuperTuxKart Website](https://supertuxkart.net/)
* [Motion control via OpenXR issue](https://github.com/supertuxkart/stk-code/issues/4382) (closed - won't fix)
